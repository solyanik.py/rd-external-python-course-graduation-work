py_ver:
	python --version

build_proj:
	{ \
	set -e ;\
	python3 -m venv env ;\
	. env/bin/activate ;\
	pip3 install -r requirements.txt ;\
	}

linter:
	pylint -d C0301 app/*.py

unittests:
	{ \
  	set -e ;\
	. env/bin/activate ;\
	nose2 --with-coverage -v ;\
	}

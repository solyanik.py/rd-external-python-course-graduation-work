from setuptools import find_packages
from setuptools import setup


setup(
    name='department-app',
    version='1.0.0',
    description='“Managing Departments and Employees” is web-application '
                'which allows users to view and record information about departments and employees of company.',
    author='Bohdan Solianyk',
    author_email='solyanik.py@gmail.com',
    url='https://gitlab.com/solyanik.py/rd-external-python-course-graduation-work',
    packages=find_packages()
)
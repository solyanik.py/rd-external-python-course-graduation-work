import time

from selenium import webdriver


def selenium_test():
    """Front-end tests with selenium"""
    path = '/home/bogdan/epam/python-online-course-home-tasks/department-app/app/driver/geckodriver'
    link = 'http://3.133.134.0:8080/home'

    driver = webdriver.Firefox(executable_path=path)
    driver.get(link)

    """add department test"""
    driver.find_element_by_id('departments_link').click()
    time.sleep(3)
    driver.find_element_by_id('add department').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Selenium test')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()

    """add existed department test"""
    time.sleep(3)
    driver.find_element_by_id('add department').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Advertise')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()

    """empty field add department test"""
    time.sleep(3)
    driver.find_element_by_id('add department').click()
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """edit department info"""
    driver.find_element_by_id('departments_link').click()
    time.sleep(3)
    driver.find_elements_by_name('Selenium test')[0].click()
    time.sleep(3)
    driver.find_element_by_id('edit').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys(' update')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """delete department test"""
    driver.find_elements_by_name('Selenium test update')[0].click()
    driver.find_element_by_id('delete').click()
    time.sleep(3)
    driver.switch_to_alert().accept()
    time.sleep(3)

    """edit existed department flash error test"""
    driver.find_elements_by_name('Purchasing')[0].click()
    driver.find_element_by_id('edit').click()
    time.sleep(3)
    driver.find_element_by_id('name').clear()
    driver.find_element_by_id('name').send_keys('Marketing')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """Add, edit and delete department test"""
    driver.find_element_by_id('add department').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Edit and Delete test')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)
    driver.find_element_by_id('edit Edit and Delete test').click()
    time.sleep(3)
    driver.find_element_by_id('name').clear()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Delete')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)
    driver.find_element_by_id('delete Delete').click()
    time.sleep(3)
    driver.switch_to_alert().accept()
    time.sleep(3)

    """add employee"""
    driver.find_element_by_id('employees_link').click()
    time.sleep(3)
    driver.find_element_by_id('add employee').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Selenium test employee')
    driver.find_element_by_id('related_department_id').send_keys('Development')
    driver.find_element_by_id('date_of_birth').send_keys('1992-02-12')
    driver.find_element_by_id('salary').send_keys('6000')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()

    """add employee with low salary"""
    time.sleep(3)
    driver.find_element_by_id('add employee').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Selenium test employee')
    driver.find_element_by_id('related_department_id').send_keys('Development')
    driver.find_element_by_id('date_of_birth').send_keys('1992-02-12')
    driver.find_element_by_id('salary').send_keys('999')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """add employee empty values"""
    driver.find_element_by_id('employees_link').click()
    driver.find_element_by_id('add employee').click()
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """edit employee"""
    driver.find_element_by_id('employees_link').click()
    driver.find_elements_by_name('Selenium test employee')[0].click()
    time.sleep(3)
    driver.find_element_by_id('edit').click()
    time.sleep(3)
    driver.find_element_by_id('name').clear()
    driver.find_element_by_id('name').send_keys('Update selenium employee')
    driver.find_element_by_id('related_department_id').send_keys('QA')
    driver.find_element_by_id('date_of_birth').clear()
    driver.find_element_by_id('date_of_birth').send_keys('1990-02-12')
    driver.find_element_by_id('salary').clear()
    driver.find_element_by_id('salary').send_keys('11500')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)

    """delete employee"""
    driver.find_elements_by_name('Update selenium employee')[0].click()
    driver.find_element_by_id('delete').click()
    time.sleep(3)
    driver.switch_to_alert().accept()
    time.sleep(3)

    """Add, edit, delete employee with employees page buttons"""
    driver.find_element_by_id('employees_link').click()
    driver.find_element_by_id('add employee').click()
    time.sleep(3)
    driver.find_element_by_id('name').send_keys('Selenium test employee 2')
    driver.find_element_by_id('related_department_id').send_keys('QA')
    driver.find_element_by_id('date_of_birth').send_keys('1988-12-12')
    driver.find_element_by_id('salary').send_keys('7777')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    time.sleep(3)
    driver.find_elements_by_id('edit Selenium test employee 2')[0].click()
    time.sleep(3)
    driver.find_element_by_id('name').clear()
    driver.find_element_by_id('name').send_keys('Selenium update employee 2')
    driver.find_element_by_id('related_department_id').send_keys('Finance')
    driver.find_element_by_id('date_of_birth').clear()
    driver.find_element_by_id('date_of_birth').send_keys('1988-12-12')
    driver.find_element_by_id('salary').clear()
    driver.find_element_by_id('salary').send_keys('7777')
    time.sleep(3)
    driver.find_element_by_id('confirm').click()
    driver.find_elements_by_id('delete Selenium update employee 2')[0].click()
    time.sleep(3)
    driver.switch_to_alert().accept()

    """Test filter by date"""
    time.sleep(3)
    driver.find_element_by_id('date_from').send_keys('1993-01-01')
    time.sleep(3)
    driver.find_element_by_id('submit').click()
    time.sleep(5)
    driver.find_element_by_id('date_from').clear()
    driver.find_element_by_id('date_by').send_keys('1986-08-08')
    time.sleep(3)
    driver.find_element_by_id('submit').click()
    time.sleep(3)
    driver.find_element_by_id('date_by').clear()
    time.sleep(5)
    driver.find_element_by_id('date_from').send_keys('1992-02-02')
    driver.find_element_by_id('date_by').send_keys('1997-01-01')
    driver.find_element_by_id('submit').click()
    time.sleep(10)

    driver.close()


if __name__ == '__main__':
    selenium_test()


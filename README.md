# Department app
#### “Managing Departments and Employees” is web-application which allows users to view and record information about departments and employees of company.

## Brief description of the project:
This Flask app with RESFUL API project created for managing departments and employees info
Here are available pages:
* Home - display departments quantity, how many employees in company and total company salary.
* Departments page display table with departments name, average departments salary. Also available add new department
or edit/delete existed departments.
* Department page show more information about certain department - how many employees in department, average and total department salary,
also available edit or delete department.
* Employees page display table with employees and their information - related department name, salary and date of birth. Available to add new employee,
edit or delete existed employees. Also available to filter employees by date of birth (in range of 2 dates) or if one date entered
will be shown employees born on a certain date.
* Employee page display info about certain employee. Available edit employee info or delete a employee.

Example of project with selenium script:

[Youtube video link](https://www.youtube.com/watch?v=RGtqV03ReYE&ab_channel=BohdanSolianyk)
![](documetation/department-app.gif)


### Database diagram

![](documetation/db%20diagram.jpeg)

### API endpoints:
Resource URL| Method'(s) | Description |
---|---|---|
`/api/departments`| GET, POST | Show existed departments, add new department |
`/api/departments/<int:department_id> `| GET, PUT, DELETE | Show existed department, update or delete it |
`/api/employees`| GET, POST | Show existed employees, add new emplyoee |
`/api/employees/<int:employee_id>` | GET, PUT, DELETE | Show existed employee, update or delete him/her |
`/api/date_filter/<string:date_from>-<string:date_by>` | GET | Filter employees in range of two dates |
`/api/date_filter/<string:date>`| GET | Search for employees born on a certain date |
`/api/average_salary` | GET | Display average salary for each department |
`/api/total_salary` | GET | Display company total salary |


Examples:
![](documetation/api1.jpeg)
![](documetation/api2.jpeg)
![](documetation/api3.jpeg)
![](documetation/api4.jpeg)

## Instructions on how to build a project from the command line:

1) Clone GitLab repository: `https://gitlab.com/solyanik.py/rd-external-python-course-graduation-work.git`
2) Create new virtual environment: `python3 -m venv v`
3) Activate virtual environment: `source env/bin/activate`
4) Install required packages: `pip install -r requirements.txt`
5) Configure your keys variables in `.env` (recommended) or `Config.py`
6) Install MySQL, init, migrate and upgrade the database with following commands in terminal:
```bash
flask db init
flask db migrate -m "Initial migration."
flask db upgrade
```
7) Run app with gunicorn: 
```gunicorn -w 3 -b 0.0.0.0:8080 run:app```
   Or run `run.py` script.
   

   `-w` (workers) must be `(2 * num_cores of your machine) + 1`, 
    find it with linux comand `nproc --all`

### Project available on Heroku - https://flask-dep-app-external-course.herokuapp.com/


### Project tasks:
- [x] Local Environment Setup:
    * System running Ubuntu Linux
    * MySQL installed
    * Git installed
    * Python installed
    * Pip installed
    * virtualenv installed
    * setuptools installed 
    * Python virtual environment configured
- [x] Create Repository:
    * Created GitLab account
    * README included
    * Features/changes were commited/pushed
- [x] Create project Software Requirements Specification (SRS):
    * Created "documentation folder"
    * Created the document SRS in Markdown format with a description of the software system 
      to be developed with all applicable use cases (mockups included).
- [x] Create HTML Prototype:
    * Created "html_prototype" folder
    * Created a static mockup version of th eapplication in HTML format with hardcoded data with hyperlinks.
- [x] Choose Python Technologies:
    * Choose the project type: Flask application
    * Choose Python technology for working with a database: SQLAlchemy
    * Used technologies: Flask, Flask-restful, Flask-marshmallow, Flask-migrate, Flask-testing, Jinja, nose2(unittest extension), coverage.
- [x] Create Project Structure:
    * Created a Python project with required structure
    ```bash
    .
    ├── ./app
    │   ├── ./app/driver
    │   ├── ./app/__init__.py
    │   ├── ./app/models
    │   ├── ./app/rest
    │   ├── ./app/service
    │   ├── ./app/static
    │   ├── ./app/templates
    │   ├── ./app/tests
    │   └── ./app/views
    ├── ./config.py
    ├── ./dist
    ├── ./documetation
    ├── ./env
    ├── ./geckodriver.log
    ├── ./MANIFEST.in
    ├── ./migrations
    ├── ./README.md
    ├── ./requirements.txt
    ├── ./run.py
    ├── ./selenium_tests.py
    └── ./setup.py
    ```
- [x] Configure CI/CD, code quality:
    * Project build configuration include "pylint" plugin 
    * Used Gitlab CI
    * Added and setup coveralls.io
- [x] Create and populate a database:
    * Created company database
    * Created two tables: "department" and "employee"
    * Populated a database with the test data
    * Created models and generate migration to manage database schema changes.
- [x] Create Web Service:
    * Created a web service (RESTful) for CRUD operations.
    * Able to deploy on Gunicorn using command line.
    * All public functions/methods on all levels include unittests.
    * Debug information displayed at the debugging level in the console and in a separate file.
    * Classes and functions include docstring comments
    * Web service has been deployed and runs on local instance
    * Web service returns data stored in the database
    * Able to make rest calls from command line and client tools (Postman)
    * Unit tests created
    * Gunicorn configured
    * Gitlab CI was successful after push
    * Jenkins pipeline
- [x] Create Web Application:
    * Created a simple web application for managing departments and employees.
    * Web app use aforementioned web service.
    * Able to deploy web app on Gunicorn using command line (Deployed on AWS - Cloud9 + Amazon MySQL RDS)
    * Public functions/methods on all levels include unittest (views have unittests for responses, front-end tested with selenium script)
    * Debug information displayed at hte debugging level.
    * Classes and functions / methods have docstrings comments.
    * README file contain a brief description of the project, instructions on how to build a project from command line,
  how to start it, and at what addresses the web service and the web application will be available after launch
- [x] Web application should allow:
    * Display a list of departments and the average salary (calculated automatically)
    * Display a list of employees in the departments with an indication of the salary for each employee and a search field to search for employees born on a certain date  or in the period between dates
    * API endpoint for average salary and date of birth filters created
    * Web application has been deployed and runs on local instance
    * Unittests created, selenium script for front-end tests 
    * Gitlab CI was successful after push
  

[![Coverage Status](https://coveralls.io/repos/gitlab/solyanik.py/rd-external-python-course-graduation-work/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/solyanik.py/rd-external-python-course-graduation-work?branch=HEAD)